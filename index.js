const express = require('express');
const app = express();
const port = 8000;
const fs = require('fs');
const path = require('path');
const bodyParser = require('body-parser');
const MongoClient = require('mongodb').MongoClient;
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
// const urlDb = 'mongodb+srv://hardieDBUser:hardiedb@hardiedb-9lt2z.mongodb.net/test?retryWrites=true&w=majority';
const urlDb = 'mongodb://localhost:27017/iteaproject';
const ObjectId = require('mongodb').ObjectID;

const OrderSchema = new Schema({
  order: {
    type: String,
    required: true
  }
})

const Order = mongoose.model('order', OrderSchema);

app.use('/', express.static('public'));
app.use(bodyParser.json());
app.use(bodyParser.text());
app.use((req, res, next) => {
  fs.readFile(path.join(__dirname, 'db.json'), (error, data) => {
    if (error) {
      console.log('error');
    }
    const db = data;
    res.locals.db = JSON.parse(db.toString());
    next();
  })
});

/* MongoClient.connect(urlDb, { useUnifiedTopology: true }, (err, client) => {
  if (err) throw new Error('Something bad happened with DB connection');
  console.log('Connected to db successfuly');
  app.listen(port, () => console.log(`Listen on port ${port}`));
  const db = client.db('iteaproject');
  const col = db.collection('orders');
  app.locals.col = col;

  process.on('SIGINT', () => {
    console.log("Close process");
    client.close();
    process.exit();
  });
}) */

mongoose.connect(urlDb,{ useNewUrlParser: true, useUnifiedTopology: true }, (err, connection) => {
  if (err) throw new Error('Something bad happened with DB connection');
  console.log('Connected to db successfuly');
  app.listen(port, () => console.log(`Listen on port ${port}`));

  process.on('SIGINT', () => {
    console.log("Close process");
    // client.close();
    process.exit();
  });
})

app.route('/orders')
  .get((req, res) => {
    Order.find({}, (err, data) => {
      if (err) throw new Error('Can not get all docs')
      res.status(200).json(data);
    })
  })
  .post((req, res) => {
    let body = { order: req.body };
    Order.create(body, (err, data) => {
      if (err) throw new Error('Can not create doc');
      res.send(`order <b>${data.order}</b> was created with id <b>${data._id}</b>`).end();
    })
  })

app.route('/orders/:id')
  .get((req, res) => {
    if (ObjectId.isValid(req.params.id)) {
      Order.find({_id: req.params.id}, (err, data) => {
        if (err) console.log(err);
        if (data[0]) {
          res.status(200).json(data[0]);
          console.log(data);
        } else {
          res.status(200).json({
            order: "There is no such item"
          })
        }
      })
    } else {
      res.status(200).json({
        order: "Enter valid id"
      })
    }
  })
  .put((req, res) => {
    let idOrder = req.params.id;
    let body = { order: req.body };
    if (ObjectId.isValid(idOrder)) {
      Order.findOneAndUpdate(
        {_id: idOrder},
        body,
        {new: true},
        (err, data) => {
          if (err) console.log(err);
          res.send(`Order <b>${data.order}</b> was updated. New order is <b>${body.order}</b>`).end()
        }
      )
    } else {
      res.send(`Enter valid ID`).end()
    }
  })
  .delete((req, res) => {
    let id = req.params.id;
    if (ObjectId.isValid(id)) {
      Order.findOneAndRemove({_id: id}, (err, data) => {
        if (err) console.log(err);
        if (data) {
          res.send(`order <b>${data.order}</b> with id ${req.params.id} was deleted`).end()
        } else {
          res.send(`No entered item`).end()
        }
      })
    } else {
      res.send(`Enter valid ID`).end();
    }
  })